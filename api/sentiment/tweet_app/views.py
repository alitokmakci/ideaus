import os
import datetime
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from google.cloud import language_v1
from msrest.authentication import CognitiveServicesCredentials
from textblob import TextBlob
from azure.cognitiveservices.language.textanalytics import TextAnalyticsClient
from .models import Tweet
import tweepy
from google_trans_new import google_translator
import json
import twint
from sentiment import settings

consumer_key = "K1CEuEa7xAWRInJfMXSyu85qG"
consumer_secret = "fdhQWpYbXLAoSLt0RIABiKWB7OlqJflqJJeAZ2D96T8PTJJs8i"
access_token = "604823919-YQtHviGUgbnONAhPxzf2NKs7M5842yaOLLA0M9Ew"
access_token_secret = "FKnEAg3XZWuCbEc56jixz3NYKHzEPZ2mJVn1bXe7ENB0i"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)
tweet__ = 0
"""
Get tweets which has given term from twitter 
"""


def get_tweets(request):
    data = json.loads(request.body.decode('utf-8'))
    term = data['term']
    tag_id = data['tag']

    if "lang" not in data:
        lang = "en"
    else:
        lang = data['lang']

    query = term + ' -filter:replies'

    tweets = api.search(q=query, lang=lang, include_entities=False, count=15, result_type='popular')
    objects = []

    for tweet in tweets:
        if hasattr(tweet, 'retweeted_status'):
            tw_obj = Tweet.objects.create(
                tweet_id=tweet.retweeted_status.id_str,
                content=tweet.retweeted_status.text,
                author_name=tweet.retweeted_status.user.name,
                author_nick=tweet.retweeted_status.user.screen_name,
                author_id=tweet.retweeted_status.user.id_str,
                retweets=tweet.retweeted_status.retweet_count,
                likes=tweet.retweeted_status.favorite_count,
                published_at=tweet.retweeted_status.created_at,
                term=term,
                tag_id=tag_id
            )
        else:
            tw_obj = Tweet.objects.create(
                tweet_id=tweet.id_str,
                content=tweet.text,
                author_name=tweet.user.name,
                author_nick=tweet.user.screen_name,
                author_id=tweet.user.id_str,
                retweets=tweet.retweet_count,
                likes=tweet.favorite_count,
                published_at=tweet.created_at,
                term=term,
                tag_id=tag_id
            )

        objects.append(tw_obj)

    objs = serializers.serialize('json', objects)

    return HttpResponse(objs, content_type="application/json")


"""
Calculate the score of given tweets
"""


def analyze_tweet(request):
    data = json.loads(request.body.decode('utf-8'))
    tweet_id = data['id']

    tweet = Tweet.objects.filter(id=tweet_id)

    tweet_obj = tweet.first()

    if tweet_obj.total_score is None:
        calculate_total_score(tweet_obj)
        tweet.first().refresh_from_db()

    tweet = serializers.serialize('json', tweet)

    return HttpResponse(tweet, content_type="application/json")


"""
 Detect the language of given tweet
"""


def detect_language(tweet):
    blob = TextBlob(tweet.content)

    lang = blob.detect_language()

    return lang


"""
Translate given tweet to English
"""


def translate_tweet(tweet):
    translator = google_translator()
    lang = detect_language(tweet)
    if lang != 'en':
        translated = translator.translate(tweet.content, lang_src=lang, lang_tgt="en")
    else:
        translated = tweet.content

    translated = str(translated)

    return translated


"""
Calculate textblob score of given tweet
"""


def calculate_textblob_score(tweet):
    tb = TextBlob(tweet)
    polarity = tb.sentiment.polarity

    return polarity


"""
Calculate google score of given tweet
"""


def calculate_google_score(tweet):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join("bitnami-qrpmi7dx6w-ff983487850d.json")
    client = language_v1.LanguageServiceClient()
    type_ = language_v1.Document.Type.PLAIN_TEXT
    document = {"content": tweet, "type_": type_, "language": "en"}
    encoding_type = language_v1.EncodingType.UTF8
    response = client.analyze_sentiment(request={'document': document, 'encoding_type': encoding_type})
    result = response.document_sentiment.score

    return result


"""
Calculate azure score of given tweet
"""


def calculate_azure_score(tweet):
    azure_score = sentiment(tweet)

    return azure_score


def authenticate_client():
    azure_key = "3290376729234ecfb3c235f18cced6d8"
    azure_endpoint = "https://ideaus.cognitiveservices.azure.com/"

    credentials = CognitiveServicesCredentials(azure_key)
    text_analytics_client = TextAnalyticsClient(
        endpoint=azure_endpoint, credentials=credentials)
    return text_analytics_client


def sentiment(text):
    client = authenticate_client()

    try:
        documents = [
            {"id": "1", "language": "en", "text": text}
        ]

        response = client.sentiment(documents=documents)

        microsoft_score = (response.documents[0].score - 0.5) * 2

        return microsoft_score

    except Exception as err:
        print("Encountered exception. {}".format(err))

        return None


"""
Calculate and optimize total score of given tweet
"""


def calculate_total_score(tweet):
    tweet_content = translate_tweet(tweet)
    google_score = calculate_google_score(tweet_content)
    azure_score = calculate_azure_score(tweet_content)
    textblob_score = calculate_textblob_score(tweet_content)

    updated_tweet = Tweet.objects.filter(id=tweet.id).update(azure_score=azure_score,
                                                             google_score=google_score,
                                                             textblob_score=textblob_score)

    tweet.refresh_from_db()
    variance = calculate_variance(tweet)

    updated_tweet = Tweet.objects.filter(id=tweet.id).update(variance=variance)

    return updated_tweet


"""
Calculate variance of tweet scores
"""


def calculate_variance(tweet):
    # Calculate sample mean of scores
    sample_mean = (tweet.google_score + tweet.azure_score + tweet.textblob_score) / 3
    # Calculate variance of scores
    variance = (
                       (tweet.google_score - sample_mean) ** 2 +
                       (tweet.azure_score - sample_mean) ** 2 +
                       (tweet.textblob_score - sample_mean) ** 2
               ) / 2

    return variance


"""
Optimize tweet related to search term
"""


def optimize_tweet(request):
    data = json.loads(request.body.decode('utf-8'))
    tweet_id = data['id']
    term = data['term']

    tweet = Tweet.objects.filter(id=tweet_id)

    optimize_score(tweet.first(), term)
    tweet.first().refresh_from_db()

    tweet = serializers.serialize('json', tweet)

    return HttpResponse(tweet, content_type="application/json")


"""
Optimize the scores of tweet
"""


def optimize_score(tweet, term):
    tweets_query = Tweet.objects.filter(term=term).order_by('variance')
    total_tweets = tweets_query.count()
    min_variance_tweet = tweets_query.first()
    min_variance = min_variance_tweet.variance

    max_variance_tweet = tweets_query[total_tweets - 1]
    max_variance = max_variance_tweet.variance

    # Calculate normalized variance
    try:
        normalized = 1 - ((tweet.variance - min_variance) / (max_variance - min_variance))
    except:
        normalized = 1

    # Calculate the optimized scores
    normalized_google_score = normalized * tweet.google_score
    normalized_azure_score = normalized * tweet.azure_score
    normalized_textblob_score = normalized * tweet.textblob_score

    # Calculate total score
    total_score = (normalized_google_score + normalized_azure_score + normalized_textblob_score) / 3

    Tweet.objects.filter(id=tweet.id).update(total_score=total_score)

    return total_score


def home(request):
    return HttpResponse(timezone.localtime(timezone.now()))


"""
Search Tweets Related To Given Term By Author
"""


def get_tweets_from_user(request):
    data = json.loads(request.body.decode('utf-8'))
    term = data['term']
    tag_id = data['tag']
    author = data['author_nick']
    dates = data['dates']

    objects = []

    for i in range(0, 12):
        c = twint.Config()
        c.Username = author
        c.Search = term
        c.Store_json = True
        c.Output = author + ".json"
        c.Since = dates[i + 1]
        c.Until = dates[i]
        c.Limit = 20

        twint.run.Search(c)
        print(len(twint.output.tweets_list))

    json_file = author + ".json"
    json_file_path = os.path.join(settings.BASE_DIR, json_file)

    tweets = []
    for line in open(json_file_path, 'r', encoding="utf8"):
        tweets.append(json.loads(line))

    counter = 0
    previous_tweet = {
        'date': '2021-01'
    }
    for tweet in tweets:
        if tweet['date'][:7] == previous_tweet['date'][:7]:
            counter += 1
        else:
            counter = 0
        if counter < 20:
            if Tweet.objects.filter(tweet_id=tweet['id']).count() == 0:
                tw_obj = Tweet.objects.create(
                    tweet_id=tweet['id'],
                    content=tweet['tweet'],
                    author_name=tweet['name'],
                    author_nick=tweet['username'],
                    author_id=tweet['user_id'],
                    retweets=tweet['retweets_count'],
                    likes=tweet['likes_count'],
                    published_at=datetime.datetime.strptime(tweet['created_at'][:19], "%Y-%m-%d %H:%M:%S").date(),
                    term=term,
                    tag_id=tag_id
                )
                objects.append(tw_obj)
        previous_tweet = tweet

    os.remove(json_file_path)

    objs = serializers.serialize('json', objects)

    return HttpResponse(objs, content_type="application/json")


def calculate_tweets(request):
    data = json.loads(request.body.decode('utf-8'))
    tag_id = data['tag_id']
    author_nick = data['author_nick']

    tweets = Tweet.objects.filter(tag_id=tag_id, author_nick=author_nick)

    for tweet in tweets:
        if tweet.total_score is None:
            calculate_total_score(tweet)
            tweet.refresh_from_db()
            optimize_score(tweet, tweet.term)

    tweets = serializers.serialize('json', tweets)

    return HttpResponse(tweets, content_type="application/json")
