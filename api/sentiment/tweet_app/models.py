from django.db import models


# Create your models here.

class Tweet(models.Model):
    content = models.TextField()
    tweet_id = models.CharField(max_length=255, null=True)
    author_name = models.CharField(max_length=255, null=True)
    author_nick = models.CharField(max_length=255, null=True)
    author_id = models.CharField(max_length=255, null=True)
    retweets = models.IntegerField(null=True)
    likes = models.IntegerField(null=True)
    google_score = models.DecimalField(max_digits=21, decimal_places=20, null=True)
    azure_score = models.DecimalField(max_digits=21, decimal_places=20, null=True)
    textblob_score = models.DecimalField(max_digits=21, decimal_places=20, null=True)
    variance = models.DecimalField(max_digits=25, decimal_places=20, null=True)
    total_score = models.DecimalField(max_digits=21, decimal_places=20, null=True)
    term = models.CharField(max_length=255, null=True)
    tag_id = models.IntegerField(null=True)
    published_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(null=True)

    class Meta:
        db_table = 'tweets'
