from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'tweet_app'
