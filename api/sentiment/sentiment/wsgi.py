"""
WSGI config for sentiment project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sentiment.settings')

application = get_wsgi_application()



{
    'ali': 10,
    'mehmet': 15,
    'mustafa': 20,
    'date': '2020-12-12',
}

{
    'ali': 10,
    'mehmet': 15,
    'mustafa': 20,
    'date': '2020-12-13',
}

{
    'ali': 10,
    'mehmet': 15,
    'mustafa': 20,
    'date': '2020-12-14',
}