(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_user-detail_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/user-detail.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/user-detail.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "user-detail",
  props: {
    author_nick: '',
    term: '',
    tag: '',
    dates: {
      type: Array
    }
  },
  data: function data() {
    return {
      tweets: [],
      loaded: 'full',
      chart: {
        series: [{
          name: 'Positive Scores',
          type: 'column',
          data: []
        }, {
          name: 'Negative Scores',
          type: 'line',
          data: []
        }],
        options: {
          chart: {
            type: 'line'
          },
          colors: ['#28a745', '#eb3f3f'],
          stroke: {
            width: [0, 4],
            curve: 'smooth'
          },
          dataLabels: {
            enabled: true,
            enabledOnSeries: [1]
          },
          labels: [],
          xaxis: {
            type: 'datetime',
            labels: {
              rotate: -45,
              format: 'yyyy MMM'
            }
          },
          yaxis: [{
            title: {
              text: 'Scores Count'
            }
          }]
        }
      }
    };
  },
  created: function created() {
    this.getUserTweets();
  },
  methods: {
    findUserTweets: function findUserTweets() {
      var _this = this;

      this.loaded = 'searching';
      this.$axios.post('http://localhost:3000/search/user', {
        term: this.term,
        tag: this.tag,
        author_nick: this.author_nick,
        dates: this.dates
      }).then(function (response) {
        var newTweets = response.data;

        if (newTweets.length > 0) {
          _this.calculate();
        } else {
          _this.getUserTweets();
        }
      });
    },
    getUserTweets: function getUserTweets() {
      var _this2 = this;

      this.loaded = 'optimizing';
      this.$axios.post('/dashboard/customer-care/customer/history', {
        tag_id: this.tag,
        author: this.author_nick
      }).then(function (response) {
        _this2.chart.options.labels = response.data.dates;
        _this2.chart.series[0].data = response.data.positive_count;
        _this2.chart.series[1].data = response.data.negative_count;
        setTimeout(function () {
          _this2.loaded = 'full';
        }, 4000);
      });
    },
    calculate: function calculate() {
      var _this3 = this;

      this.loaded = 'calculating';
      this.$axios.post('http://localhost:3000/tag', {
        tag_id: this.tag,
        author_nick: this.author_nick
      }).then(function (response) {
        _this3.getUserTweets();
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/components/user-detail.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/user-detail.vue ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _user_detail_vue_vue_type_template_id_232d8e18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user-detail.vue?vue&type=template&id=232d8e18& */ "./resources/js/components/user-detail.vue?vue&type=template&id=232d8e18&");
/* harmony import */ var _user_detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user-detail.vue?vue&type=script&lang=js& */ "./resources/js/components/user-detail.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _user_detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _user_detail_vue_vue_type_template_id_232d8e18___WEBPACK_IMPORTED_MODULE_0__.render,
  _user_detail_vue_vue_type_template_id_232d8e18___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/user-detail.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/user-detail.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/user-detail.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_user_detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./user-detail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/user-detail.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_user_detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/user-detail.vue?vue&type=template&id=232d8e18&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/user-detail.vue?vue&type=template&id=232d8e18& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_user_detail_vue_vue_type_template_id_232d8e18___WEBPACK_IMPORTED_MODULE_0__.render,
/* harmony export */   "staticRenderFns": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_user_detail_vue_vue_type_template_id_232d8e18___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_user_detail_vue_vue_type_template_id_232d8e18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./user-detail.vue?vue&type=template&id=232d8e18& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/user-detail.vue?vue&type=template&id=232d8e18&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/user-detail.vue?vue&type=template&id=232d8e18&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/user-detail.vue?vue&type=template&id=232d8e18& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* binding */ render,
/* harmony export */   "staticRenderFns": () => /* binding */ staticRenderFns
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-md-12 text-center w-100 mb-5" }, [
      _c(
        "button",
        { staticClass: "btn btn-primary", on: { click: _vm.findUserTweets } },
        [_vm._v("Search Customer History")]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-12 mb-5" }, [
      _vm.loaded === "searching"
        ? _c("div", { staticClass: "text-center w-100" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("h5", [
              _vm._v(
                "\n                Searching Customer History...\n            "
              )
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.loaded === "calculating"
        ? _c("div", { staticClass: "text-center w-100" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("h5", [
              _vm._v("\n                Calculating Scores...\n            ")
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.loaded === "optimizing"
        ? _c("div", { staticClass: "text-center w-100" }, [
            _vm._m(2),
            _vm._v(" "),
            _c("h5", [
              _vm._v("\n                Optimizing Scores...\n            ")
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.loaded === "full"
        ? _c(
            "div",
            [
              _c("h5", { staticClass: "text-center" }, [
                _vm._v(
                  "\n                Visualized Customer History Chart\n            "
                )
              ]),
              _vm._v(" "),
              _c("apexchart", {
                attrs: {
                  type: "line",
                  options: _vm.chart.options,
                  series: _vm.chart.series
                }
              })
            ],
            1
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border text-primary", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Searching...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border text-danger", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Calculating...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border text-success", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Optimizing...")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => /* binding */ normalizeComponent
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);