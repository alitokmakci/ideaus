(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_termCharts_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/termCharts.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/termCharts.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../app */ "./resources/js/app.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "termCharts",
  data: function data() {
    return {
      positiveWords: [],
      negativeWords: [],
      chart1: {
        options: {
          legend: {
            position: 'bottom'
          },
          labels: ['Positive', 'Negative', 'Neutral'],
          colors: ['#28a745', '#eb3f3f', '#5780f7'],
          noData: {
            text: 'There is no data available',
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0
          }
        },
        series: []
      },
      chart2: {
        options: {
          legend: {
            position: 'bottom'
          },
          labels: ['Positive', 'Negative', 'Neutral'],
          colors: ['#28a745', '#eb3f3f', '#5780f7'],
          noData: {
            text: 'There is no data available',
            align: 'center',
            verticalAlign: 'middle',
            offsetX: 0,
            offsetY: 0
          }
        },
        series: []
      },
      chart3: {
        ready: false,
        options: {
          xaxis: {
            categories: []
          },
          grid: {
            show: false
          },
          colors: ['#28a745', '#eb3f3f', '#5780f7'],
          chart: {
            type: 'bar',
            stacked: true,
            stackType: '100%'
          },
          stroke: {
            width: 1,
            colors: ['#fff']
          },
          legend: {
            position: 'bottom',
            horizontalAlign: 'left',
            offsetX: 40
          }
        },
        series: [{
          name: 'Positive Scores',
          data: []
        }, {
          name: 'Negative Scores',
          data: []
        }, {
          name: 'Neutral Scores',
          data: []
        }]
      }
    };
  },
  props: {
    tag_id: ''
  },
  created: function created() {
    var _this = this;

    this.getChart1();
    this.getChart2();
    this.getChart3();
    this.getPositiveWords();
    this.getNegativeWords();
    _app__WEBPACK_IMPORTED_MODULE_0__.bus.$on('optimized', function () {
      _this.getChart1();

      _this.getChart2();

      _this.getChart3();

      _this.getPositiveWords();

      _this.getNegativeWords();
    });
  },
  methods: {
    getChart1: function getChart1() {
      var _this2 = this;

      this.$axios.post('/dashboard/chart1', {
        tag_id: this.tag_id
      }).then(function (response) {
        _this2.chart1.series = response.data;
      });
    },
    getChart2: function getChart2() {
      var _this3 = this;

      this.$axios.post('/dashboard/chart2', {
        tag_id: this.tag_id
      }).then(function (response) {
        _this3.chart2.series = response.data;
      });
    },
    getChart3: function getChart3() {
      var _this4 = this;

      this.$axios.post('/dashboard/chart3', {
        tag_id: this.tag_id
      }).then(function (response) {
        _this4.chart3.series[0].data = response.data.positive_count;
        _this4.chart3.series[1].data = response.data.negative_count;
        _this4.chart3.series[2].data = response.data.neutral_count;
        _this4.chart3.options.xaxis.categories = response.data.dates;
        _this4.chart3.ready = true;
      });
    },
    getPositiveWords: function getPositiveWords() {
      var _this5 = this;

      this.$axios.get("/dashboard/positive-word-count?tag_id=".concat(this.tag_id)).then(function (response) {
        _this5.positiveWords = response.data;
      });
    },
    getNegativeWords: function getNegativeWords() {
      var _this6 = this;

      this.$axios.get("/dashboard/negative-word-count?tag_id=".concat(this.tag_id)).then(function (response) {
        _this6.negativeWords = response.data;
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/components/termCharts.vue":
/*!************************************************!*\
  !*** ./resources/js/components/termCharts.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _termCharts_vue_vue_type_template_id_23574828_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./termCharts.vue?vue&type=template&id=23574828&scoped=true& */ "./resources/js/components/termCharts.vue?vue&type=template&id=23574828&scoped=true&");
/* harmony import */ var _termCharts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./termCharts.vue?vue&type=script&lang=js& */ "./resources/js/components/termCharts.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _termCharts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _termCharts_vue_vue_type_template_id_23574828_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _termCharts_vue_vue_type_template_id_23574828_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "23574828",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/termCharts.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/termCharts.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/termCharts.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => __WEBPACK_DEFAULT_EXPORT__
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_termCharts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./termCharts.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/termCharts.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_termCharts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/termCharts.vue?vue&type=template&id=23574828&scoped=true&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/termCharts.vue?vue&type=template&id=23574828&scoped=true& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_termCharts_vue_vue_type_template_id_23574828_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
/* harmony export */   "staticRenderFns": () => /* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_termCharts_vue_vue_type_template_id_23574828_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_termCharts_vue_vue_type_template_id_23574828_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./termCharts.vue?vue&type=template&id=23574828&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/termCharts.vue?vue&type=template&id=23574828&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/termCharts.vue?vue&type=template&id=23574828&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/termCharts.vue?vue&type=template&id=23574828&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => /* binding */ render,
/* harmony export */   "staticRenderFns": () => /* binding */ staticRenderFns
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("h5", [_vm._v("The Emotions of Recent Tweets:")]),
      _vm._v(" "),
      _c("apexchart", {
        staticClass: "mb-5",
        attrs: {
          width: "600",
          type: "donut",
          options: _vm.chart1.options,
          series: _vm.chart1.series
        }
      }),
      _vm._v(" "),
      _c("h5", [_vm._v("The Emotions of All Tweets:")]),
      _vm._v(" "),
      _c("apexchart", {
        staticClass: "mb-5",
        attrs: {
          width: "600",
          type: "donut",
          options: _vm.chart2.options,
          series: _vm.chart2.series
        }
      }),
      _vm._v(" "),
      _c("h5", [_vm._v("The Emotions of All Tweets By Date:")]),
      _vm._v(" "),
      _vm.chart3.ready
        ? _c("apexchart", {
            staticClass: "mb-5",
            attrs: {
              type: "bar",
              options: _vm.chart3.options,
              series: _vm.chart3.series
            }
          })
        : _vm._e(),
      _vm._v(" "),
      _c("h5", { staticClass: "text-center" }, [
        _vm._v("Most Used Words In Positive Tweets")
      ]),
      _vm._v(" "),
      _c("vue-word-cloud", {
        staticClass: "mx-auto mb-5",
        staticStyle: { height: "480px", width: "640px" },
        attrs: {
          words: _vm.positiveWords,
          color: function(ref) {
            var weight = ref[1]

            return weight > 10
              ? "#064E3B"
              : weight > 5
              ? "#059669"
              : weight > 2
              ? "#34D399"
              : "#A7F3D0"
          },
          "font-family": "Poppins",
          "enter-animation": { opacity: 0, transform: "scale3d(0.3,0.3,0.3)" }
        },
        scopedSlots: _vm._u([
          {
            key: "default",
            fn: function(ref) {
              var text = ref.text
              var weight = ref.weight
              var word = ref.word
              return [
                _c(
                  "div",
                  {
                    staticStyle: { cursor: "pointer" },
                    attrs: { title: weight }
                  },
                  [
                    _vm._v(
                      "\n                " + _vm._s(text) + "\n            "
                    )
                  ]
                )
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c("h5", { staticClass: "text-center" }, [
        _vm._v("Most Used Words In Negative Tweets")
      ]),
      _vm._v(" "),
      _c("vue-word-cloud", {
        staticClass: "mx-auto mb-5",
        staticStyle: { height: "480px", width: "640px" },
        attrs: {
          words: _vm.negativeWords,
          color: function(ref) {
            var weight = ref[1]

            return weight > 10
              ? "#7F1D1D"
              : weight > 5
              ? "#DC2626"
              : weight > 2
              ? "#F87171"
              : "#FECACA"
          },
          "font-family": "Poppins",
          "enter-animation": { opacity: 0, transform: "scale3d(0.3,0.3,0.3)" }
        },
        scopedSlots: _vm._u([
          {
            key: "default",
            fn: function(ref) {
              var text = ref.text
              var weight = ref.weight
              var word = ref.word
              return [
                _c(
                  "div",
                  {
                    staticStyle: { cursor: "pointer" },
                    attrs: { title: weight }
                  },
                  [
                    _vm._v(
                      "\n                " + _vm._s(text) + "\n            "
                    )
                  ]
                )
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => /* binding */ normalizeComponent
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);