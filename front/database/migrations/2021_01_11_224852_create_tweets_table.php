<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->id();
            $table->longText('content')->nullable();
            $table->string('term')->nullable();
            $table->string('tweet_id')->nullable();
            $table->string('author_name')->nullable();
            $table->string('author_nick')->nullable();
            $table->string('author_id')->nullable();
            $table->integer('retweets')->nullable();
            $table->integer('likes')->nullable();
            $table->decimal('google_score', 21, 20)->nullable();
            $table->decimal('azure_score', 21, 20)->nullable();
            $table->decimal('textblob_score', 21, 20)->nullable();
            $table->decimal('variance', 21, 20)->nullable();
            $table->decimal('total_score', 21, 20)->nullable();
            $table->unsignedBigInteger('tag_id')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->foreign('tag_id')->on('tags')->references('id')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
