@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h3 class="text-center p-0 pb-3">
                        <span class="float-left">Customer List of tag: <strong
                                class="text-primary">{{ $tag->term }}</strong></span>
                        <delete-button tag="{{ $tag->id }}"></delete-button>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            @foreach($tweets as $tweet)
                                <div class="col-md-3">
                                    <a target="_blank" href="{{ route('customer-detail', ['tweet' => $tweet->id]) }}">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    {{ $tweet->author_name }}
                                                </h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
