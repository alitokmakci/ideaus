<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{{ env('APP_NAME') }} - Customer Relation Management System</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/landing.css">
    @livewireStyles
</head>
<body>
<div id="app">
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item is-size-5 is-bold" href="/">
                    {{ env('APP_NAME') }}
                </a>

                <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="menu">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="menu" class="navbar-menu">
                <div class="navbar-end">
                    <a href="#" class="navbar-item">
                        Home
                    </a>
                    <a href="#solutions" class="navbar-item">
                        Solutions
                    </a>
                    <a href="#features" class="navbar-item">
                        Features
                    </a>
                    <div class="navbar-item">
                        <div class="buttons">
                            <a href="#signup" class="button is-info">
                                <strong>Sign up</strong>
                            </a>
                            <a href="{{ route('login') }}" class="button is-light">
                                Log in
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <section class="hero has-background-light">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-mobile is-multiline">
                    <div class="column is-full-mobile is-two-fifths-desktop">
                        <h1 class="title is-size-1 has-text-weight-lighter pt-6 mt-6 has-text-centered-mobile has-text-centered-tablet-only">
                            {{ env('APP_NAME') }}
                        </h1>
                        <h2 class="subtitle has-text-weight-light has-text-centered-mobile has-text-centered-tablet-only">
                            Focus experiences and opinions of your customers and their expectations.
                        </h2>
                        <div class="is-flex-tablet-only is-flex-mobile">
                            <button class="button is-info is-rounded mx-auto">LEARN MORE</button>
                        </div>
                    </div>
                    <div class="column">
                        <figure class="image is-4by3">
                            <img src="/img/hero.jpg" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section" id="solutions">
        <div class="container py-6">
            <h1 class="has-text-centered is-size-3 has-text-weight-lighter">Using {{ env('APP_NAME') }} to track your
                customer
                experience is easy</h1>
            <h2 class="has-text-centered subtitle has-text-weight-medium has-text-grey mb-6">
                “Your most unhappy customers are your greatest source of learning.” – Bill Gates
            </h2>
            <div class="columns is-mobile is-multiline">
                <div class="column is-full-mobile is-one-third-tablet has-text-centered">
                    <figure class="image is-96x96 mx-auto">
                        <img src="/img/coc.png" alt="">
                    </figure>
                    <h1 class="is-size-5 has-text-black mt-3">Customer Opinion Calculator (COC)</h1>
                    <p>Find out what your customers think.</p>
                </div>
                <div class="column is-one-third-tablet is-full-mobile  has-text-centered">
                    <figure class="image is-96x96 mx-auto">
                        <img src="/img/ccs.png" alt="">
                    </figure>
                    <h1 class="is-size-5 has-text-black mt-3">Customer Care System (CCS)</h1>
                    <p>Increase your customer's loyalty.</p>
                </div>
                <div class="column is-full-mobile is-one-third-tablet has-text-centered">
                    <figure class="image is-96x96 mx-auto">
                        <img src="/img/cre.png" alt="">
                    </figure>
                    <h1 class="is-size-5 has-text-black mt-3">Churn Risk Estimator (CRE)</h1>
                    <p>Track your churn risk and take precautions.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="features" class="section has-background-light">
        <div class="container py-6">
            <h1 class="has-text-centered is-size-3 has-text-weight-lighter">
                Better relationships with your customers
            </h1>
            <h2 class="has-text-centered subtitle has-text-weight-medium has-text-grey mb-6">
                We think that in today's business world the most important think is customer experiences.
            </h2>
            <div class="columns is-mobile is-multiline pt-6 mt-6">
                <div class="column is-full-mobile is-half-tablet has-text-centered">
                    <canvas id="chart1"></canvas>
                </div>
                <div class="column is-full-mobile is-half-tablet has-text-centered-mobile">
                    <h1 class="title">We are visualization your customers opinions</h1>
                    <p class="subtitle has-text-weight-light">
                        <b>{{ env('APP_NAME') }}</b> let you see what your customers feelings about your product or your
                        service.
                        We designed a flexible dashboard to increase your customer communication.
                    </p>
                </div>
            </div>
            <div class="columns is-mobile is-multiline pt-6 mt-6">
                <div class="column is-full-mobile is-half-tablet has-text-centered-mobile">
                    <h1 class="title">
                        See the development of your customer relationships
                    </h1>
                    <p class="subtitle has-text-weight-light">
                        With <b>{{ env('APP_NAME') }}</b> you will receive reports in the specific
                        periods and you will be able to easily view your progress.
                    </p>
                </div>
                <div class="column is-full-mobile is-half-tablet has-text-centered">
                    <canvas id="chart2"></canvas>
                </div>
            </div>
        </div>
    </section>
    @livewire('register')
    <footer class="footer has-background-grey-darker">
        <div class="container">
            <div class="content has-text-centered">
                <h1 class="is-size-2 has-text-white has-text-left mb-0">
                    {{ env('APP_NAME') }}
                </h1>
                <p class="has-text-left has-text-white has-text-left has-text-weight-lighter">
                    Visualize your customers experiences...
                </p>
                <p class="has-text-grey-light is-size-7">All rights reserved. 2020 &copy;</p>
            </div>
        </div>
    </footer>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script src="/js/landing.js"></script>
@livewireScripts
</body>
</html>
