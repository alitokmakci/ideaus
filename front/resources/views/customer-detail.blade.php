@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h3 class="text-center p-0 pb-3">
                        <span class="float-left">Detail of customer: <strong
                                class="text-primary">{{ $tweet->author_name }}</strong></span>
                        <delete-tweet-button tag_id="{{ $tweet->tag->id }}" author_id="{{ $tweet->author_id }}"></delete-tweet-button>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <user-detail :dates="{{ json_encode($dates) }}" author_nick="{{ $tweet->author_nick }}" term="{{ $tweet->term }}" tag="{{ $tweet->tag->id }}"></user-detail>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
