<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>{{ env('APP_NAME') }} </title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/plugins/font-awesome/css/all.min.css" rel="stylesheet">


    <!-- Theme Styles -->
    <link href="/assets/css/lime.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('style')
</head>
<body>
<div id="app">

    <div class="lime-sidebar">
        <div class="lime-sidebar-inner slimscroll">
            <ul class="accordion-menu">
                <li class="sidebar-title">
                    Apps
                </li>
                <li>
                    <a href="{{ route('dashboard') }}" {{ Request::route()->getName() == 'dashboard' ? 'class=active' : '' }}><i class="material-icons">person_outline</i><small>Customer Opinion Calculator</small></a>
                </li>
                <li>
                    <a href="{{ route('customer-care-system') }}" {{ Request::route()->getName() == 'customer-care-system' ? 'class=active' : '' }}><i class="material-icons">loyalty</i><small>Customer Care System</small></a>
                </li>
                <li>
                    <a href="todo.html"><i class="material-icons">warning</i><small>Churn Risk Estimator</small></a>
                </li>
                <li class="sidebar-title">
                    Products & Services
                </li>
                @if (auth()->user()->tags()->exists())
                    @foreach(auth()->user()->tags as $tag)
                        <li>
                            <a href="{{ route('tag-detail', ['tag' => $tag->id]) }}"><i class="material-icons">label</i>{{ $tag->term }}</a>
                        </li>
                    @endforeach
                @else
                    <li>
                        <span class="text-danger"><strong><small>*Your Tags Will Be Listed Here</small></strong></span>
                    </li>
                @endif
                <li class="sidebar-title">
                    Other
                </li>
                <li>
                    <a href="#"><i class="material-icons">bookmark_border</i>Documentation</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="lime-header">
        <nav class="navbar navbar-expand-lg">
            <section class="material-design-hamburger navigation-toggle">
                <a href="javascript:void(0)" class="button-collapse material-design-hamburger__icon">
                    <span class="material-design-hamburger__layer"></span>
                </a>
            </section>
            <a class="navbar-brand" href="{{ route('dashboard') }}">{{ env('APP_NAME') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="material-icons">keyboard_arrow_down</i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <search></search>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="dropdown-item" href="/logout">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="lime-container">
        <div class="lime-body">
            @yield('content')
        </div>
        <div class="lime-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span class="footer-text">2021 © {{ env('APP_NAME') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Javascripts -->
<script src="/assets/plugins/jquery/jquery-3.1.0.min.js"></script>
<script src="/assets/plugins/bootstrap/popper.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/js/lime.min.js"></script>
<script src="/js/app.js"></script>
@yield('js')
</body>
</html>
