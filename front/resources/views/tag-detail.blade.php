@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h3 class="text-center pl-0">
                        <span class="float-left">Details of tag: <strong class="text-primary">{{ $tag->term }}</strong></span>
                        <search-button term="{{ $tag->term }}"></search-button>
                        <delete-button tag="{{ $tag->id }}"></delete-button>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4>Recently Founded Tweets:</h4>
                @if (count($recentTweets) == 0)
                    No tweets founded or searched recently
                @else
                    @foreach($recentTweets as $tweet)
                        <tweet :tweet="{{ $tweet }}"></tweet>
                    @endforeach
                @endif
                <hr>
                <h4>Tweets Founded in Past Searches:</h4>
                @foreach($pastTweets as $tweet)
                    <tweet :tweet="{{ $tweet }}"></tweet>
                @endforeach
            </div>
            <div class="col-md-6">
                <h4>Analysis Results:</h4>
                <div class="card">
                    <div class="card-body text-center">
                        <term-charts tag_id="{{ $tag->id }}"></term-charts>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .apexcharts-canvas {
            margin-left: auto;
            margin-right: auto;
        }
    </style>

@endsection
