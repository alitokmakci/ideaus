<section id="signup" class="section has-background-grey-darker">
    <form wire:submit.prevent="registerUser">
        <div class="container has-text-centered">
            <h1 class="has-text-centered has-text-white-bis is-size-3 has-text-weight-lighter">
                Create an account on <b>{{ env('APP_NAME') }}</b>
            </h1>
            <h2 class="has-text-centered has-text-white subtitle has-text-weight-light mb-6 is-size-6">
                Sign up to <b>{{ env('APP_NAME') }}</b> and get your 14 days free trial.
            </h2>
            <div class="columns is-mobile is-multiline">
                <div class="column is-full-mobile is-one-quarter-tablet">
                    <div class="field">
                        <div class="control is-expanded has-text-left">
                            <label class="label has-text-white">Name:</label>
                            <input wire:model="name" class="input" id="register_name" minlength="6" required type="text" name="name" placeholder="Username">
                        </div>
                    </div>
                </div>
                <div class="column is-full-mobile is-one-quarter-tablet">
                    <div class="field">
                        <div class="control is-expanded has-text-left">
                            <label class="label has-text-white">Email:</label>
                            <input wire:model="email" class="input" id="register_email" required type="email" name="email" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div class="column is-full-mobile is-one-quarter-tablet">
                    <div class="field">
                        <div class="control is-expanded has-text-left">
                            <label class="label has-text-white">Password:</label>
                            <input wire:model="password" class="input" id="register_password" minlength="6" required type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                </div>
                <div class="column is-full-mobile is-one-quarter-tablet">
                    <div class="field">
                        <div class="control">
                            <label class="label">Go</label>
                            <button type="submit" id="register_button" class="button is-info is-fullwidth">
                                Start
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
