@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card bg-success text-white">
                    <div class="card-body">
                        <div class="dashboard-info row">
                            <div class="info-text col-md-6">
                                <h5 class="card-title">Welcome to <strong>Customer Care
                                        System</strong>, {{ auth()->user()->name }}!</h5>
                                <p>Get familiar with the app, here are some ways to get started.</p>
                                <ul>
                                    <li>Add your products/services from the search bar above</li>
                                    <li>Check customer stats for your products/services bellow</li>
                                    <li>Read your reports</li>
                                </ul>
                            </div>
                            <div class="info-image col-md-6"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a class="text-dark" href="{{ route('dashboard') }}">
                    <div class="card stat-card">
                        <div class="card-body">
                            <h5 class="card-title">Customer Opinion Calculator</h5>
                            <div class="progress" style="height: 10px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 100%"
                                     aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="text-dark" href="{{ route('customer-care-system') }}">
                    <div class="card stat-card">
                        <div class="card-body">
                            <h5 class="card-title">Customer Care System</h5>
                            <div class="progress" style="height: 10px;">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 100%"
                                     aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="text-dark" href="#" id="churn">
                    <div class="card stat-card">
                        <div class="card-body">
                            <h5 class="card-title">Churn Risk Estimator</h5>
                            <div class="progress" style="height: 10px;">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 100%"
                                     aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            @foreach($tags as $tag)
                <div class="col-md-4">
                    <a class="text-dark" target="_blank" href="{{ route('customer-tag-detail', ['tag' => $tag->id]) }}">
                        <div class="card stat-card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $tag->term }}</h5>
                                <h2 class="float-right">{{ \App\Http\Controllers\PanelController::positiveTweetsCounter($tag->tweets) }}%</h2>
                                <p>Positive Scores:</p>
                                <div class="progress" style="height: 10px;">
                                    <div class="progress-bar bg-success" role="progressbar"
                                         style="width: {{ \App\Http\Controllers\PanelController::positiveTweetsCounter($tag->tweets) }}%"
                                         aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('js')
    <script src="/assets/plugins/toastr/toastr.min.js"></script>
    <script>
        $('#churn').on('click', function () {
            toastr.error('This app is not available for now')
        })
    </script>

@endsection

@section('style')
    <link href="/assets/plugins/toastr/toastr.min.css" rel="stylesheet">
@endsection
