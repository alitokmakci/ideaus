// Toggle mobile menu
document.querySelector('.burger').addEventListener('click', function (e) {

    let menu = '#' + e.target.dataset.target

    document.querySelector(menu).classList.toggle('is-active')
})

//Example charts
const cart1 = document.getElementById("chart1").getContext("2d");
const cart2 = document.getElementById("chart2").getContext("2d");
let marksData = {
    labels: ["Screen", "Sound", "Touch", "Battery", "Camera", "Brightness"],
    datasets: [{
        label: "Positive Opinions",
        data: [65, 75, 70, 80, 60, 80],
        backgroundColor: "rgba(49,116,220,0.1)",
        borderColor: "rgb(49,116,220)",
    }, {
        label: "Negative Opinions",
        data: [54, 65, 60, 70, 70, 75],
        backgroundColor: "rgba(255,71,15,0.1)",
        borderColor: "rgb(255,71,15)",
    }]
};
let speedData = {
    labels: ["May", "June", "July", "August", "September", "October"],
    datasets: [{
        label: "Screen Resolution",
        data: [10, 59, 75, 20, 20, 55],
        fill: false,
        borderColor: "rgb(255,71,15)"
    }, {
        label: "Fast Charging",
        data: [56, 37, 45, 19, 65, 43],
        fill: false,
        borderColor: "rgb(49,116,220)"
    }]
};
const radarChart = new Chart(cart1, {
    type: 'radar',
    data: marksData,
    responsive: true,
});

const lineChart = new Chart(cart2, {
    type: 'line',
    data: speedData,
    options: {
        fill: false
    }
});
