import Vue from 'vue'
import axios from "axios";
import VueApexCharts from 'vue-apexcharts'
import VueSweetalert2 from 'vue-sweetalert2'
import VueWordCloud from 'vuewordcloud';

Vue.component(VueWordCloud.name, VueWordCloud);
Vue.use(VueApexCharts)
Vue.use(VueSweetalert2)

Vue.component('apexchart', VueApexCharts)

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
Vue.prototype.$axios = axios

export const bus = new Vue()

const app = new Vue({
    el: '#app',
    components: {
        search: () => import('./components/searchBox'),
        searchButton: () => import('./components/search-button'),
        deleteButton: () => import('./components/delete-button'),
        deleteTweetButton: () => import('./components/delete-tweet-button'),
        tweet: () => import('./components/tweet'),
        termCharts: () => import('./components/termCharts'),
        userDetail: () => import('./components/user-detail')
    }
});
