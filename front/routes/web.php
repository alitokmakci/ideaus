<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome')->name('welcome')->middleware('guest');
Route::get('/login', 'UserController@login')->name('login')->middleware('guest');
Route::post('/login', 'UserController@loginPost')->middleware('guest');
Route::get('/logout', 'UserController@logout')->name('logout')->middleware('auth');

Route::group(['prefix' => '/dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', 'PanelController@dashboard')->name('dashboard');
    Route::group(['prefix' => '/customer-care', 'middleware' => 'auth'], function () {
        Route::get('/', 'PanelController@customerCareSystem')->name('customer-care-system');
        Route::get('/tag/{tag}', 'PanelController@showCustomerCareSystemTag')->name('customer-tag-detail');
        Route::get('/customer/{tweet}', 'PanelController@showCustomerDetail')->name('customer-detail');
        Route::post('/customer/history', 'PanelController@customerHistory')->name('customer-history');
    });
    Route::get('/tag/{tag}', 'TagController@show')->name('tag-detail');
    Route::post('/tag', 'TagController@store');
    Route::delete('/tag', 'TagController@destroy');
    Route::post('/tweet', 'TweetController@show');
    Route::put('/tweet', 'TweetController@update');
    Route::delete('/tweet', 'TweetController@destroy');
    //Charts
    Route::post('/chart1', 'ChartController@chart1');
    Route::post('/chart2', 'ChartController@chart2');
    Route::post('/chart3', 'ChartController@chart3');
    Route::get('/positive-word-count', 'PanelController@positiveTweetsWordCount');
    Route::get('/negative-word-count', 'PanelController@negativeTweetsWordCount');

});

