<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Http\Services;


use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserService
 * @package App\Http\Services
 */
class UserService
{
    /**
     * @param $credentials
     * @return User|false
     */
    public function create($credentials)
    {
        $user = new User();

        $user->name = $credentials['name'];
        $user->email = $credentials['email'];
        $user->password = bcrypt($credentials['password']);

        $user->save();

        if ($user) {
            Auth::login($user);

            return $user;
        }

        return false;
    }

    /**
     * @param $credentials
     * @return User|false|\Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function login($credentials)
    {
        if (Auth::attempt($credentials)) {
            return Auth::user();
        }

        return false;
    }
}
