<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $tweet = Tweet::find($request->input('id'));

        if ($tweet->created_at == null) {
            $tweet->created_at = Carbon::now();
            $tweet->updated_at = Carbon::now();
            $tweet->save();
        }

        return response($tweet);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Tweet $tweet
     * @return \Illuminate\Http\Response
     */
    public function edit(Tweet $tweet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tweet = Tweet::find($request->input('id'));

        if ($tweet->created_at == null) {
            $tweet->created_at = Carbon::now();
            $tweet->updated_at = Carbon::now();
            $tweet->save();
        }

        return response($tweet);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Tweet::where('tag_id', $request->input('tag_id'))
            ->where('author_id', $request->input('author_id'))
            ->delete();

        return response('', 200);
    }
}
