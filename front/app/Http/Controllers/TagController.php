<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->tags->contains('term', $request->input('term'))) {
            $tag = Auth::user()->tags()->where('term', $request->input('term'))->first();

            return response($tag);
        }

        $tag = new Tag();
        $tag->term = $request->input('term');
        $tag->user_id = Auth::id();
        $tag->report_period = 'monthly';

        $tag->save();

        return response($tag);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        foreach ($tag->tweets as $tweet) {
            if ($tweet->created_at == null) {
                $tweet->created_at = Carbon::now();
                $tweet->updated_at = Carbon::now();

                $tweet->save();
            }
        }

        $recentTweets = $tag->tweets()->whereDate('created_at', '>=', \Carbon\Carbon::today())->orderBy('created_at', 'DESC')->get();
        $pastTweets = $tag->tweets()->whereDate('created_at', '<', \Carbon\Carbon::today())->orderBy('created_at', 'DESC')->get();

        return response(view('tag-detail')->with('tag', $tag)->with('pastTweets', $pastTweets)->with('recentTweets', $recentTweets));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $tag = Tag::find($request->input('tag'));

        $tag->delete();

        return response(['completed' => true]);
    }
}
