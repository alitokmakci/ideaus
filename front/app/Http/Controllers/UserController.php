<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login()
    {
        return view('login');
    }

    public function loginPost(LoginRequest $request)
    {
        $user = $this->userService->login($request->only('email', 'password'));

        if ($user) {
            return redirect(route('dashboard'));
        }

        return view('login')->with('error', 'Given credentials are wrong...');
    }

    public function logout()
    {
        Auth::logout();

        return redirect(route('welcome'));
    }
}
