<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Tweet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function chart1(Request $request)
    {
        $tag = Tag::find($request->input('tag_id'));
        $tweets = $tag->tweets()->select('total_score')->whereTime('created_at', '>', \Carbon\Carbon::now()->subHour())->whereNotNull('total_score')->get();

        $positives = 0;
        $negatives = 0;
        $neutrals = 0;

        foreach ($tweets as $tweet) {
            if ($tweet->total_score > 0.2) {
                $positives++;
            } elseif ($tweet->total_score < -0.2) {
                $negatives++;
            } else {
                $neutrals++;
            }
        }

        return response([$positives, $negatives, $neutrals]);
    }

    public function chart2(Request $request)
    {
        $tag = Tag::find($request->input('tag_id'));
        $tweets = $tag->tweets()->select('total_score')->whereNotNull('total_score')->get();

        $positives = 0;
        $negatives = 0;
        $neutrals = 0;

        foreach ($tweets as $tweet) {
            if ($tweet->total_score > 0.2) {
                $positives++;
            } elseif ($tweet->total_score < -0.2) {
                $negatives++;
            } else {
                $neutrals++;
            }
        }

        return response([$positives, $negatives, $neutrals]);
    }

    public function chart3(Request $request)
    {
        $startDate = Carbon::now()->subYear();
        $finishDate = Carbon::now();

        $dates = [];
        $positiveScores = [];
        $negativeScores = [];
        $neutralScores = [];

        while ($startDate <= $finishDate) {
            $positiveTweets = Tweet::where('tag_id', $request->input('tag_id'))
                ->whereMonth('published_at', $startDate->month)
                ->whereYear('published_at', $startDate->year)
                ->where('total_score', '>', 0.2)
                ->count();

            $negativeTweets = Tweet::where('tag_id', $request->input('tag_id'))
                ->whereMonth('published_at', $startDate->month)
                ->whereYear('published_at', $startDate->year)
                ->where('total_score', '<', -0.2)
                ->count();

            $neutralTweets = Tweet::where('tag_id', $request->input('tag_id'))
                ->whereMonth('published_at', $startDate->month)
                ->whereYear('published_at', $startDate->year)
                ->where('total_score', '<', 0.2)
                ->where('total_score', '>', -0.2)
                ->count();

            array_push($dates, $startDate->format('F Y'));
            array_push($positiveScores, $positiveTweets);
            array_push($negativeScores, $negativeTweets);
            array_push($neutralScores, $neutralTweets);

            $startDate = $startDate->addMonth();
            $positiveTweets = null;
            $negativeTweets = null;
            $neutralTweets = null;
        }

        return response(['dates' => $dates, 'positive_count' => $positiveScores, 'negative_count' => $negativeScores, 'neutral_count' => $neutralScores]);
    }
}
