<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Http\Livewire;

use App\Http\Services\UserService;
use Livewire\Component;

/**
 * Class Register
 * @package App\Http\Livewire
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */
class Register extends Component
{
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $password;

    /**
     * @var string[]
     */
    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email',
        'password' => 'required|min:6'
    ];

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.register');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function registerUser()
    {
        $this->validate();

        (new UserService())->create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
        ]);

        return redirect('/dashboard');
    }
}
