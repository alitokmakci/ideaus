<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tweet
 *
 * @property int $id
 * @property string|null $content
 * @property string|null $tweet_id
 * @property string|null $author_name
 * @property string|null $author_nick
 * @property string|null $author_id
 * @property int|null $retweets
 * @property int|null $likes
 * @property string|null $google_score
 * @property string|null $azure_score
 * @property string|null $textblob_score
 * @property string|null $variance
 * @property string|null $total_score
 * @property int|null $tag_id
 * @property int $search_id
 * @property string|null $published_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereAuthorNick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereAzureScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereGoogleScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereRetweets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereSearchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereTextblobScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereTotalScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereVariance($value)
 * @mixin \Eloquent
 * @property string|null $term
 * @property-read \App\Models\Tag|null $tag
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereTerm($value)
 */
class Tweet extends Model
{
    use HasFactory;

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
